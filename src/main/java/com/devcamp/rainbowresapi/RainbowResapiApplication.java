package com.devcamp.rainbowresapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowResapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowResapiApplication.class, args);
	}

}
