package com.devcamp.rainbowresapi.controllers;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowColor(){
        ArrayList<String> color = new ArrayList<>();

        color.add("red");
        color.add("orange");
        color.add("yellow");
        color.add("green");
        color.add("indigo");
        color.add("red");
        color.add("violet");
        
        return color;
    }
}
